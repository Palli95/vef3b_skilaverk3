

var sUrl = 'http://apis.is/concerts';

    var tmplSource = $('#tmpl-midi').html(),
      template = Handlebars.compile(tmplSource);

    $.getJSON(sUrl).done(function(results) { 
        $(".midi-list").html(template(results));

console.log(results)


  var $imgs = $('#gallery div');          // Get the images
  var $search = $('#filter-search');      // Get the input element
  var cache = [];                       // Create an array called cache

  $imgs.each(function() {                 // For each image
    cache.push({                          // Add an object to the cache array
      element: this,                      // This image
      text: this.title.trim().toLowerCase() // Its alt text (lowercase trimmed)
    });
  });

  function filter() {                     // Declare filter() function
    var query = this.value.trim().toLowerCase();  // Get the query
    cache.forEach(function(div) {         // For each entry in cache pass image 
      var index = 0;                      // Set index to 0

      if (query) {                        // If there is some query text
        index = div.text.indexOf(query);  // Find if query text is in there
      }

      div.element.style.display = index === -1 ? 'none' : '';  // Show / hide


    });
  }

  if ('oninput' in $search[0]) {          // If browser supports input event
    $search.on('input', filter);          // Use input event to call filter()
  } else {                                // Otherwise
    $search.on('keyup', filter);          // Use keyup event to call filter()
  }





console.log(cache)


obj = JSON.stringify(cache);
console.log(obj)




  var $imgs = $('#gallery #myndir');                  // Store all images
  var $buttons = $('#buttons');                   // Store buttons element
  var tagged = {};                                // Create tagged object

  $imgs.each(function() {                         // Loop through images and
    var img = this;                               // Store img in variable
    var tags = $(this).data('tags2');              // Get this element's tags

    if (tags) {                                   // If the element had tags
      tags.split(',').forEach(function(tagName) { // Split at comma and
        if (tagged[tagName] == null) {            // If object doesn't have tag
          tagged[tagName] = [];                   // Add empty array to object
        }
        tagged[tagName].push(img);                // Add the image to the array
      });
    }
  });

  $('<button/>',  {                                 // Create empty button
    text: 'Show All',                              // Add text 'show all'
    class: 'active', 
                               // Make it active
    click: function() {                            // Add onclick handler to
      $(this)                                      // Get the clicked on button
        .addClass('active')                        // Add the class of active
        .siblings()                                // Get its siblings
        .removeClass('active');                    // Remove active from siblings
      $imgs.show();                                // Show all images
    }
  }).appendTo($buttons);                           // Add to buttons

  $.each(tagged, function(tagName) {               // For each tag name
    $('<button/>', {                               // Create empty button
      text: tagName + ' (' + tagged[tagName].length + ')',
      id:tagName,   
       // Add tag name
      click: function() {                          // Add click handler
        $(this)                                    // The button clicked on
          .addClass('active')                      // Make clicked item active
          .siblings()                              // Get its siblings
          .removeClass('active');                  // Remove active from siblings
        $imgs                                      // With all of the images
          .hide()                                  // Hide them
          .filter(tagged[tagName])                 // Find ones with this tag
          .show();                                 // Show just those images
      }
    }).appendTo($buttons);       
        

document.getElementById(tagName).onclick = function() { 
  if(tagName == "Græni")
  {
   mapDisplay(65.681183, -18.089533);
  }
  if(tagName == "Bæjarbíó")
  {
   mapDisplay(64.069517, -21.957817);
  }
  if(tagName == "Hljóðberg")
  {
   mapDisplay(64.144217, -21.935667);
  }
  if(tagName == "Ferðafélagssalurinn")
  {
   mapDisplay(64.1131, -21.852383);
  }
  if(tagName == "Tjarnarbíó")
  {
   mapDisplay(64.14595, -21.943733);
  }
  if(tagName == "Hljómahöllin")
  {
   mapDisplay(64.13605, -21.855417);
  }
 
  if(tagName == "Grafarvogskirkja")
  {
   mapDisplay(64.132233, -21.799917);
  }
  if(tagName == "Langholtskirkja")
   {
   mapDisplay(64.1353, -21.858183);
   }
   if(tagName == "Austurbær")
   {
   mapDisplay(64.14215, -21.9171);
   marker.setMap(map);
   }
   if(tagName == "Hilton")
   {
   mapDisplay(64.1397, -21.889);
   marker.setMap(map);
   }
  
};    


  });



 });






function mapDisplay(latval, lngval) {
    
  var myLatlng = new google.maps.LatLng(-25.363882,131.044922);
    
    var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(latval, lngval),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: 'Hello World!'
  });
    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
};

// 





/*

{
  "results": [
    {
      "eventDateName": "Vínartónleikar 2015",
      "name": "Græn - tónleikaröð",
      "dateOfShow": "2015-01-08T19:30:00",
      "userGroupName": "Harpa",
      "eventHallName": "Eldborg                                           ",
      "imageSource": "http://midi.is/images/medium/15.759.jpg"
    },
    {
      "eventDateName": "Föstudagsfreistingar 09.01",
      "name": "",
      "dateOfShow": "2015-01-09T12:00:00",
      "userGroupName": "Miðasala MAk",
      "eventHallName": "Hamrar                                            ",
      "imageSource": "http://midi.is/images/medium/1.8394.jpg"
    }
  ]
}
*/
