var sUrl = 'http://apis.is/currency/arion';

// jQuery aðferð
$.getJSON(sUrl, function(data){
    var x;
    for (x in data.results) {
        $('.currencytable').append(buaTilHtml(data.results[x].shortName, data.results[x].longName, data.results[x].bidValue, data.results[x].askValue));
    }
});

var buaTilHtml = function(shortName, currency, sell, buy){
    return "<tr><td>" + shortName + "</td><td>" + currency + "</td><td>" + sell + "</td><td>" + buy + "</td></tr>";
}


/* JSON skrá sem er sótt og unnið með 
{
  "results": [
    {
      "shortName": "USD",
      "longName": "Bandarískur dalur",
      "value": 121.515,
      "askValue": 121.88,
      "bidValue": 121.15,
      "changeCur": -0.40857,
      "changePer": 0
    },
    {
      "shortName": "EUR",
      "longName": "Evra",
      "value": 162.185,
      "askValue": 162.67,
      "bidValue": 161.7,
      "changeCur": 0.09845,
      "changePer": "0.00"
    }
  ]
}

 */ 